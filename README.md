# MicroPython SpaceCAN

SpaceCAN is an onboard system communication bus that is highly reliable.

Please refer to the official [SpaceCAN documentation](https://librecube.gitlab.io/standards/spacecan/).

## Installation

Copy the `spacecan` folder (located in the `src` folder) to the root directory of the pyboard.

## Example

See the [examples folder](examples/README.md) on how to create and operate a SpaceCAN network.

## Differences to Python SpaceCAN

The [Python SpaceCAN implementation](https://gitlab.com/librecube/lib/python-spacecan) is the reference implementation of SpaceCAN.

This MicroPython implementation differs slightly, in the following source files:

- src/spacecan/controller.py
- src/spacecan/responder.py
- src/spacecan/primitives/heartbeat.py
- src/spacecan/primitives/network.py
- src/spacecan/primitives/sync.py
- src/spacecan/primitives/timer.py
- src/spacecan/services/core.py
- src/spacecan/services/ST01_request_verification.py
- src/spacecan/services/ST03_housekeeping.py
- src/spacecan/services/ST08_function_management.py
- src/spacecan/services/ST17_test.py
- src/spacecan/services/ST20_parameter_management.py
- src/spacecan/transport/*

## Contribute

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube documentation](https://librecube.gitlab.io).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
