import time
import micropython
import pyb
import spacecan


micropython.alloc_emergency_exception_buf(100)
led_heartbeat = pyb.LED(1)


def received_telemetry(data, node_id):
    print(f"telemetry received [{data}] from node {node_id}")


controller = spacecan.Controller.from_file("config/controller.json")
controller.received_telemetry = received_telemetry
controller.sent_heartbeat = led_heartbeat.toggle
controller.connect()
controller.start()

INTRO_TEXT = """
    <Stop program with Ctrl-C>

    Enter one of following command where x is destination node id:
        b: bus switch
        s: send scet
        u: send utc
        tx: send dummy telecommand frame to node x     

    Enter 'h' to show this text again.
"""
print(INTRO_TEXT)

try:
    while True:
        x = input(">> ").lower()

        if x == "h":
            print(INTRO_TEXT)

        elif x == "b":
            print("switch bus")
            controller.switch_bus()

        elif x == "s":
            coarse_time = int(time.ticks_ms())
            fine_time = 1
            controller.send_scet(coarse_time, fine_time)

        elif x == "u":
            day = int(time.time() / 60 / 60 / 24)  # days since 1970-01-01
            ms_of_day = 1
            sub_ms_of_day = 2
            controller.send_utc(day, ms_of_day, sub_ms_of_day)

        elif x.startswith("t"):
            node_id = int(x[1:])
            data = [1, 2, 3, 4, 5, 6, 7, 8]
            controller.send_telecommand(data, node_id)

except KeyboardInterrupt:
    print()

controller.stop()
controller.disconnect()
